// ============================
// Main Controller
// ============================


//Initialize location data
const attractions = [
  ["Merlion", 1.286920, 103.854570, 9],
  ["AsianCivilisationsMuseum", 1.287466, 103.851424, 8],
  ["ClarkeQuay", 1.290555, 103.846188, 7],
  ["FortCanningPark", 1.295526, 103.845331, 6],
  ["OrchardRoad", 1.302279, 103.837399, 5],
  ["SingaporeFlyer", 1.289332, 103.863152, 4],
  ["MarinaBaySands", 1.283099, 103.860295, 3],
  ["GardensByTheBay", 1.281790, 103.863954, 2],
  ["Chinatown", 1.284193, 103.843362, 1],
]

// Copied from Google Maps Doc
// Initialize and add the map
function initMap() {
  const merlion = { lat: 1.286920, lng: 103.854570 };
  // The map, centered at Merlion Singapore
  const map = new google.maps.Map(document.getElementById("map"), {
    zoom: 15,
    center: merlion,
  });

  initMarkers(map);

  google.maps.event.addListener(map, 'click', function () {
    map.setZoom(15);
    map.setCenter(merlion);
  })
}
// Copied from Google Maps Doc

// Add initial marker
function initMarkers(map) {

  var theIcon = '../../img/icons/ic_location.png';

  var cstIcon = {
    url: theIcon,
    size: new google.maps.Size(120, 60),
    scaledSize: new google.maps.Size(70, 60),
    origin: new google.maps.Point(8, -8)
  }

  for (let i = 1; i < attractions.length; i++) {
    const attraction = attractions[i];
    let attr = attraction[0];

    // Define marker
    let marker = new google.maps.Marker({
      map,
      position: { lat: attraction[1], lng: attraction[2] },
      icon: cstIcon,
      optimized: false,
      title: attraction[0],
      label: {
        text: attraction[0],
        className: 'custom-label',
        color: 'white'
      }
    });

    // Set marker event listener
    google.maps.event.addListener(marker, 'click', function () {
      map.setZoom(17);
      map.setCenter({ lat: attraction[1], lng: attraction[2] });
      getData(attr, changePlace)
    })
  }
}

// Function to Change Attraction Description
function changePlace(placeData) {
  // Assert to html
  document.getElementById("place-name").innerText = placeData.name;
  document.getElementById("place-intro").innerText = placeData.intro;
  document.getElementById("place-desc").innerText = placeData.desc;
  document.getElementById("place-web").innerHTML = `<span><img src="./img/icons/ic_globe.png" /></span>
  ${placeData.web}`;
  document.getElementById("place-img").src = placeData.img;
  document.getElementById("place-addr").innerText = placeData.addr;
}

// Function to get JSON data
function getData(_placeName, callback) {
  $.get('../../__mocks__/location.json', function (data) {
    let placeData = data[_placeName];
    callback(placeData);
  });
}


initMap();
